"""Example code"""
import argparse
import logging
import requests
import pprint
import unittest
from unittest.mock import patch


def request_crypto():
    r = requests.get("https://random-data-api.com/api/crypto/random_crypto")
    data = {}
    if r.status_code == 200:
        print("Got crypto.")
        data = r.json()
        pprint.pprint(data)
    return data


class TestAPIConnection(unittest.TestCase):

    def test_dict_len(self):
        data = request_crypto()
        self.assertEqual(len(data), 5)

    @patch('__main__.request_crypto')
    def test_sha_len(self, mock_request_crypto):
        mock_request_crypto.return_value = {
            "sha1": "sakjhdak",
            "sha256": "kjbfda091nlnda"
        }
        data = mock_request_crypto()
        sha1_len = len(data.get("sha1"))
        sha256_len = len(data.get("sha256"))
        self.assertLess(sha1_len, sha256_len)

def mock_demo():
    """
    >>> mock_demo()
    Traceback (most recent call last):
      File "<stdin>", line 1, in <module>
    TypeError: 'NonCallableMock' object is not callable
    """
    from unittest.mock import NonCallableMock
    property = NonCallableMock()
    property()


# logging.basicConfig(format=f'[%(levelname)s][{__name__}] %(message)s', level=logging.INFO)

def main():
    args = parse_args()
    log = get_logger(args.verbose)
    log.info("Attention! I'm about to greet user!")
    for i in range(args.repeat):
        if args.silent:
            log.debug("Will be silent")
        else:
            log.info("Greeting user.")
            print(f"Hi, {args.name}")


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='A greeting program.')
    parser.add_argument('--verbose', action="store_true",
                        help='Enables debug mode.')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-s', '--silent', action="store_true",
                        help='remain quiet.')
    group.add_argument('-n', '--name',
                        help='a name to be greeted with.')
    parser.add_argument('-r', dest="repeat", type=int, default=1,
                        help='Number of times to repeat greeting.')

    args = parser.parse_args()
    return args


def get_logger(verbose):
    log_level = logging.DEBUG if verbose else logging.INFO
    logger = logging.getLogger(f'{__name__}')
    logger.setLevel(log_level)
    ch = logging.StreamHandler()
    ch.setLevel(log_level)
    formatter = logging.Formatter('[%(levelname)s][%(asctime)s][%(name)s] - %(message)s')
    ch.setFormatter(formatter)
    logger.addHandler(ch)
    return logger


if __name__ == '__main__':
    unittest.main()
    # mock_demo()
    # main()
