import requests
import re
import pprint

r = requests.get("https://www.ultrasound-images.com/metabolic-diseases/")
if r.status_code == 200:
    html = r.text
    pattern = r"<a\srel=\"lightbox\[(?P<jakies_numerki>\d+)\]\"\shref=\"(?P<image_url>https://www\.ultrasound-images\.com/s/cc_images/cache_\d+\.[a-zA-Z]{3,4}).+"
    links = re.findall(pattern, html)
    pprint.pprint(links)
else:
    print("Failed to fetch website")
