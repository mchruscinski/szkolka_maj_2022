import threading
import time

class HelloPrinter(threading.Thread):
    """Thread printing Hello string."""
    def run(self, repeats: int = 10, sleep: int = 10) -> None:
        for _ in range(repeats):
            print("Hello!")
            time.sleep(sleep)


class WorldPrinter(threading.Thread):
    """Thread printing World string."""
    def run(self, repeats: int = 5, sleep: int = 20) -> None:
        for _ in range(repeats):
            print("World")
            time.sleep(sleep)


def function_printer(repeats: int = 20, sleep: int = 5) -> None:
    """Function printing Hello function! string"""
    for _ in range(repeats):
        print("Hello function!")
        time.sleep(sleep)


def threads_demo() -> None:
    """Demonstration of thread objects."""
    hello_printer = HelloPrinter()
    world_printer = WorldPrinter()
    f_printer = threading.Thread(target=function_printer, kwargs={"repeats": 20, "sleep": 5})

    hello_printer.start()
    world_printer.start()
    f_printer.start()

    print("Main thread will sleep now.")
    time.sleep(30)

    print("Joining all threads.")
    hello_printer.join()
    world_printer.join()
    f_printer.join()
    print("El fin.")

if __name__ == "__main__":
    threads_demo()
