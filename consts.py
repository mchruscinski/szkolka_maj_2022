"""TUI constants for displaying doors."""

STATISTICS_FILENAME = "stats.json"

StrategyStats = dict[str, int]
GameStats = dict[str, StrategyStats]
STATISTICS_TEMPLATE: GameStats = {
    "zmien": {
        "wygrane": 0,
        "przegrane": 0,
    },
    "zostaw": {
        "wygrane": 0,
        "przegrane": 0,
    }
}

DOOR = """
+------+
|      |
|   {}  |
|      |
|      |
|      |
+------+"""

GOAT = """
+------+
|  ((  |
|  oo  |
| /_/|_|
|    | |
|GOAT|||
+------+"""

CAR = """
+------+
| CAR! |
|    __|
|  _/  |
| /_ __|
|   O  |
+------+"""
