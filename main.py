"""Projekt Monty Hall"""
import json
import os
import random
from typing import Optional

from consts import DOOR, GOAT, CAR, STATISTICS_FILENAME, \
    STATISTICS_TEMPLATE, GameStats


def main() -> None:
    """Main function"""
    statistics = read_statistics()
    game_on = True
    while game_on:
        strategy, win = game_round()
        if strategy is not None and win is not None:
            result_key = "wygrane" if win else "przegrane"
            statistics[strategy][result_key] += 1
        print_statistics(statistics)
        game_on = ask_continue()
    write_statistics(statistics)


def read_statistics() -> GameStats:
    """Read statistics from file."""
    if not os.path.exists(STATISTICS_FILENAME):
        write_statistics(STATISTICS_TEMPLATE)
    with open(STATISTICS_FILENAME, "r", encoding="utf-8") as file_handler:
        raw = file_handler.read()
    statistics: GameStats = json.loads(raw) if raw else STATISTICS_TEMPLATE
    return statistics


def write_statistics(statistics: GameStats) -> None:
    """Write statistics to file."""
    with open(STATISTICS_FILENAME, "w", encoding="utf-8") as file_handler:
        file_handler.write(json.dumps(statistics))


def print_statistics(statistics: GameStats) -> None:
    """Print game statistics."""
    change_wins = statistics["zmien"]["wygrane"]
    change_losses = statistics["zmien"]["przegrane"]
    print(f"Zmiana, wygrane: {change_wins}, przegrane: {change_losses}")
    stay_wins = statistics["zostaw"]["wygrane"]
    stay_losses = statistics["zostaw"]["przegrane"]
    print(f"Pozostanie, wygrane: {stay_wins}, przegrane: {stay_losses}")


def ask_continue() -> bool:
    """Ask to continue game."""
    keep_asking = True
    decision = "n"
    while keep_asking:
        decision = input("Chcesz grac dalej? [Y/N] > ").lower()
        if decision in ["y", "n"]:
            keep_asking = False
        else:
            print("Blad! Wpisz 'Y' lub 'N'!")
    return decision == "y"


def game_round() -> tuple[Optional[str], Optional[bool]]:
    """Play single round of the game."""
    strategy = None
    win = None
    door_state = [False, False, False]
    door_num = len(door_state)
    door_choices: list[int] = list(range(1, door_num + 1))
    winning_door = random.randint(1, len(door_state))

    print_door(door_state, winning_door)
    user_choice = get_user_input()

    if user_choice:
        reveal_index = get_reveal_number(door_choices, winning_door, user_choice)
        door_state[reveal_index - 1] = True
        print_door(door_state, winning_door)

        print(f"Obecnie obstawiasz bramke: {user_choice}")
        door_choices.remove(reveal_index)
        strategy = switch_or_stay()
        if strategy == "zmien":
            door_choices.remove(user_choice)
            user_choice = door_choices[0]

        door_state = [True, True, True]
        print_door(door_state, winning_door)
        print(f"Ostatecznie wybrales bramke: {user_choice}")
        if win := user_choice == winning_door:
            print("Gratulacje! Wygrales!")
        else:
            print("Niestety, odchodzisz do domu z koza.")
    return strategy, win


def print_door(is_open: list[bool], winning_index: int) -> None:
    """Prints doors next to each other."""
    clear_screen()
    print("TELETURNIEJ MONTY HALL!")
    doors = []
    lines = []
    for i in range(3):
        if not is_open[i]:
            doors.append(DOOR.format(i+1).split("\n"))
        elif i == (winning_index - 1):
            doors.append(CAR.split("\n"))
        else:
            doors.append(GOAT.split("\n"))
    for each in zip(doors[0], doors[1], doors[2]):
        lines.append(" ".join(each))
    for line in lines:
        print(line)


def get_user_input() -> Optional[int]:
    """Reads user input"""
    read_input = True
    chosen_gate = None
    while read_input:
        user_input = input("Podaj numer bramki 1-3 > ")
        if user_input == "q":
            read_input = False
        else:
            read_input, chosen_gate = parse_gate_choice(user_input)
    return chosen_gate


def parse_gate_choice(user_input: str) -> tuple[bool, Optional[int]]:
    """Parse input from user into gate number"""
    chosen_gate = None
    read_input = True
    try:
        chosen_gate = int(user_input)
        if 1 <= chosen_gate <= 3:
            read_input = False
        else:
            print("Błąd! Musisz wybrać z zakresu 1-3!")
    except (ValueError, TypeError):
        print("Błąd! Podaj numer bramki 1-3 lub 'q' by wyjsc.")
    return read_input, chosen_gate


def switch_or_stay() -> str:
    """Prompts user for switch or stay decision."""
    print("Zmieniasz bramke, czy zostajesz przy pierwotnym wyborze?")
    read_input = True
    user_decision = ""
    short_mapping = {
        "y": "zmien",
        "n": "zostaw"
    }
    while read_input:
        user_decision = input("Czy zmieniasz bramke? [Y/N] > ").lower()
        if user_decision in short_mapping:
            user_decision = short_mapping[user_decision]
            read_input = False
        else:
            print("Błąd! Napisz: 'zmien' lub 'zostaw'!")
    return user_decision


def get_reveal_number(door_choice: list[int], winning_door: int, user_choice: int) -> int:
    """Determines which door will be revealed."""
    reveal_list = door_choice.copy()
    reveal_list.remove(winning_door)
    if winning_door != user_choice:
        reveal_list.remove(user_choice)
    return random.choice(reveal_list)


def clear_screen() -> None:
    """Clears screen, cross OS."""
    if os.name == "nt":
        command = "cls"
    else:
        command = "clear"
    os.system(command)


if __name__ == "__main__":
    main()
